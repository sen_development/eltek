#include "extcode.h"
#pragma pack(push)
#pragma pack(1)

#ifdef __cplusplus
extern "C" {
#endif
typedef struct {
	int32_t dimSize;
	Path CellString[1];
} PathArrayBase;
typedef PathArrayBase **PathArray;
typedef struct {
	LVBoolean status;
	int32_t code;
	LStrHandle source;
} Cluster;

/*!
 * Create_zip
 */
void __cdecl Create_zip(Path *selectedPath, PathArray *ArrayOfPathToZip, 
	Cluster *errorOut);

MgErr __cdecl LVDLLStatus(char *errStr, int errStrLen, void *module);

/*
* Memory Allocation/Resize/Deallocation APIs for type 'PathArray'
*/
PathArray __cdecl AllocatePathArray (int32 elmtCount);
MgErr __cdecl ResizePathArray (PathArray *hdlPtr, int32 elmtCount);
MgErr __cdecl DeAllocatePathArray (PathArray *hdlPtr);

#ifdef __cplusplus
} // extern "C"
#endif

#pragma pack(pop)

