﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="15008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Device.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../../Lib/Device.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.4</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"]&lt;5F.31QU+!!.-6E.$4%*76Q!!'KQ!!!2P!!!!)!!!'IQ!!!!A!!!!!AR%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-!!!#1&amp;1#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)H73M1.L!V.B(+B*OZ\LA-!!!!-!!!!%!!$!!$&amp;X!7Q,HL"1L6A/$=&lt;0#JFV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!;UGP?`(=ZU[4G50V[&lt;45M1%!!!$`````!!!!%)F`EI7Y6\QBIJCF3Z#N&amp;8I!!!!%!!!!!!!!!))!!5R71U-!!!!#!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*735.$!!!!!!%/2'6W;7.F6(FQ:3ZD&gt;'Q!5&amp;2)-!!!!"5!!1!$!!!/2'6W;7.F6(FQ:3ZD&gt;'Q!!!!#!!(`!!!!!1!"!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!$!!!!!!!#!!)!!!!!!#-!!!!;?*RD9'&gt;A&lt;G#YQ!$%D!Z-$5Q:1.9("A;/!!9!3H5&amp;I!!!!!!5!!!!$(C=9W"BY',A!%-'!!%+!#=!!!"*!!!"'(C=9W$!"0_"!%AR-D!QP103,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XMM+%A?\?![3:1(*1.=)1+;&lt;&lt;1(Q#X2R_+(U"31Q!L-=J/1!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'9!!!$1(C=7]$)Q*"J&lt;'(W!5AT!\%I1Q.$=HZ++C]$E-]!!7_9'%A'!6$^7GDCBA=/JQ'"(L^]#ZD@`);HWU6&amp;I,F'29+J6+4&lt;2U7EUU?&amp;J:.&amp;Z=7@````.R`B/&gt;TNE80=U1;ENJM$+(\=295$R!(3,#$[@W!'3"7K?4+&gt;1&amp;EA,9'E!7YACPU"1&amp;5=$28+$#5MBA?C$B^P-''%/"4GB#BM\C8?`/9X(%"0#2R]S.,&gt;K!(E^UY%E5!BHMY1$IHD,BQ[9E!_YQG1A:U]-&amp;^TQ0U4"D+A2%7AUQ2E%1MDT+*ONO-/'O"Q="#"5"E1KA*#&amp;9#I(7!8(/')/QQ0`\7P\_VC"&gt;,)57I!R#$V?AS-$)RA/5;'N6!Z'S#&lt;#3I'CUM17Q%;4)Q-^H!^N[(S'EDGO$$#^#$5630:SQ1WAZ(B$Q0-0+"^5$UA.\&amp;"R8S"9A?A\"!A?Q+5(1VE@Y#SEY"M!3A\%]AW9)3Q]["M:X]86_3U#%L8M$1O$M4*O15'"HL64DKJY4&lt;G&gt;DL"%&amp;D,!)KBZ),E-CR3$!#2+JCC!!!"/!!!!&gt;RYH$.A9'$).,9Q#W"E9'!'9F''"I&lt;E`*25"C3QAB("$AU/$WN_)^$NIK,3[;,#U_WDIN(JI],29-H!0`61AQ710.RRT_#;Q?53&lt;PZNRZI`-,[]#^46']$9';,#UVP*W&amp;GDQJ(7\=#9:OX)S$`F!(`L'J"U%6.H#6![HKET2I7DWYGJWZ]*;#J,*YP+CT````^P`A&amp;2WQJ37]=#.)/H.ZOF-Q?IVIWFWZM&amp;5WVJ5G]Z#^D-7";QG?YMX&lt;\9V.HXOL'!0.,L$+*QG8?1@^N*@J=4H3&gt;?#`&amp;P/`#;&amp;U2QA!BG).&amp;]2#!O0DLW-$B]VL[_NQM58%B"RG!!R%I-%AQM1*I*C!OBYC$W8#4W534W7S3^)/$M\_++(C=A/]3"/$GXQ-"!L^J**T8=RNR/*RA#;RE!GOG"1!!!!0Y!!!'=?*QT9'"AS$3W-".A:'"A"G*2BA;'Z0S56!9EE-')9!?(BT7`%?BW56(I&gt;&amp;(B[@:25?HU5?(I9OBA",*Z?BU:Q=)O+BS^!9S&gt;)3I=;&gt;W?D'G\W"D3GH_SJ`%\85TL$'!%3P0U6D"VVA"6FKBQ."^D[A23X1YACK742?8&amp;H````\=?+%XJD7@JD&amp;(B;4\'!F&lt;ATI+GQ,GXGA6E#ES"-\I#J6[A(B@=*P#\('A_)B!8(XU9ZL_VL_`N!HE8S=M-"E#="22B!&gt;*-1!Q,("#\$9G^()F^!EEP#$D\O\CCBSH)$D%A4CZ),N/L&gt;N**$&lt;=RN^-*BM";"A9!$G*F'!!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!-&amp;1#!!!!$"$%V,D!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!-&amp;1#!!!!$"$%V,D!!!!!!$B5"A!=!!Q9R.3YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!&amp;"1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!&amp;L6G$L15!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!&amp;L6EP,S]PA[U&amp;!!!!!!!!!!!!!!!!!!!!!0``!!!&amp;L6EP,S]P,S]P,Y/N"1!!!!!!!!!!!!!!!!!!``]!AVEP,S]P,S]P,S]P,S_$L1!!!!!!!!!!!!!!!!$``Q":73]P,S]P,S]P,S]P,`[$!!!!!!!!!!!!!!!!!0``!&amp;G$AVEP,S]P,S]P,`\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY.:,S]P,`\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$7;X_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q#$AY/$AY/$A`\_`P\_`I/$!!!!!!!!!!!!!!!!!0``!!":79/$AY/$`P\_`I/N71!!!!!!!!!!!!!!!!!!``]!!!!!79/$AY0_`I/$71!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!&amp;G$AY/$,Q!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!":,Q!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!#!!!!!!"[!!&amp;'5%B1!!!!!1!#6%2$1Q!!!!%/2'6W;7.F6(FQ:3ZD&gt;'Q!5&amp;2)-!!!!"5!!1!$!!!/2'6W;7.F6(FQ:3ZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!#L5&amp;2)-!!!!!!!!!!!!!-!!!!!"L=!!"5=?*S^7&amp;VM5W59@L_T&gt;JSO(4U&gt;'VM8I.UY,1MQR#E`%F1CJ_$9(W0]$%S5UBZ:47F(W[&amp;I!F[5S1Q%%R)3*60%B"MP#"*#.*CI'7$3#]F-.)**A1NO#*%&lt;AC*H^@X/4]`JT\9+9STZ=D,?Z`V\HP?=^RN!42V8RYT"!1E)^Q!@WC7I#+9)Q%AT#_I`\S"Q7]C`1+K&gt;2))V\"&lt;O"D.'ZEBA#[;]\&amp;*B#/[D&gt;?:GZD3ZRHT+X5,4=M[*TCIEM!&gt;4N9Y.@*LDT]\BB]S;VVH1SRUF9UQP\XL)(EV'-3!EG_DJ;#:D1)1'EWGE=?9[-2*0C,&amp;&gt;9EQ-"@K30$7Q.,./W&lt;N&amp;!EZ)6=&lt;Y^!*UDFH]*(NH\J!U]V$T$OC^#=[@0[_$(!L)+W?U%D%E$5$O-X=GQ&amp;1*K&lt;FR0LV9RF4)')RT8YMDO-;/UT)I+"][3UBR#%8=?WLV&amp;+PBJ$5S\P&lt;NWYD$5]8V36$.JV^DH?SNY(6T:8@S)B!A)W_RG;(-?IJX&gt;&amp;*#:.MKZ-3TCH!^_.QDQ=*ECNE**CX7!PB?:M3E-&lt;);'3(L:%9MQB!(/C58*K&amp;%1%LGL1U05%L=`I'YOSXMD]@&amp;3(]MN-_@#0I49K31IJ?&amp;F'5&amp;,:^'EY5#N7!C]_&amp;&lt;9\OD=/&lt;-'?Q!HDLU&amp;947]OEM&lt;J:;DN&lt;SI.ZS'F6PX;P9/O(PT&lt;NJ_TSL'%W`Z63`Z+#MYK`F:\.],J*`YZ00VX6VN[#[(6R_UDHK8P:-V&lt;U=28=A4^VQA&gt;DASA2+8;'!$/KW9&gt;PG[O17Q;R%T-%=&gt;&gt;MQTFQNTE4K@KF1X49N6F&lt;&gt;*U[=S-%B2;OS[D92IKA\?3XT/0/9;PT(T/_E"R3.+\XO2G;I`R1'_!?#O3RM(6@20204Y#Y(2&gt;7W(D%7]I=\"`9A'1I2H%=:-$1%PIFALJYM.V9*NAEJ*^:A6U+S.'1^G-!,&amp;6DP(^@`R(I("E]Z8-LE+&gt;VY_$0&amp;K]^8$=^8$-_87;@&amp;J=@J6?,M5_,]I#I2&amp;MJR--+5R-%?&lt;M&gt;:;_(46M/9/O5Y/T5O-Q[:SU?0(G&amp;-0*5?*BPA1Z-JS&gt;0@Z,;-S7E:3G6(XDD89*?8)*?4DP-&lt;K"80&gt;P$M!"TKSZY0''7OS\S$:;J2%!?;4\E6&amp;A"]K%#KE$+NOH/;1I3TLAW[2+QIE9W43)22*6)BC0N#!8(T`HYR&lt;V+X38!SG$*R)+2&gt;'[BD"`\P)D7&amp;'6I+Q`26U:EDUMR@G!%$V36FQ$&gt;#8NS&lt;%HS/=:XY`KEQ+(]'8)1[O*J(W_DI;!&amp;NT!3U*2/?*5P@6C.63P!&amp;[B$V940IQQRWG!XH3AAUE4\Y4]D)ECV7)&lt;1\F0#(X;X0&gt;67N'YA%%K&amp;IR,V?D)AR@S);M`P#9C!2CU:#!8&gt;\V"_M\)I(XA_&amp;Q^&amp;Y).IPFAE&gt;(&lt;;.U8@RK^1TU.]@XF__31T\^]@6\'U3H-,MF`.J2MZ_;@9&amp;91-&lt;:HTJUC8-'%^ZCBC;8ZF\R+7&lt;O'74"HJSD;J9!WSD-ETZJP2&gt;VU$08.0ML'%W8SK^,$.E-R0R^5_?T;\3M^G6EQW/Z7H-RDC7"%6:L\`#V&lt;'5;=U&gt;S[_-9SEK-WHS$D*?.XA&lt;-NZ'=.T&amp;#M"S6Y&gt;]9Y"5NCE1-X[?$V8`OO?\EX?/K::&gt;_*87NMJ\=G&gt;7Z\TRS3%[T_PF9;L2R^G-YTRPUBUGW&gt;A;\WT.GS7&lt;2!;6._"7W;6&gt;&gt;FE(]`'H"=O@VY:_[+G`;@677]'/L;9G$@4-NPK&gt;1F95UW#'GO)ZO?H4?T5IDXS%.=\GUT8[2G7'*FB2),O]!GOQ"]6E&amp;SJ-24%NF.WYJE`PV6DA978&lt;X'YAE&gt;&lt;9$+O.[UOR'OOR$7\:J)'?W2"#94:&amp;4?FC6:LF`X&gt;KL(")K&lt;$85#&amp;^?23LE/2'=+&amp;*M74[=E,U3?2D?8X?:&amp;(GMRTH=`".3I&amp;KU9U&lt;MS;AP?BX7.X(MJ_[Y_L8&gt;F0"0N:?QAX$UB%.CO&amp;/`Z\]\ST/`@0D&lt;HJ0Z&gt;EKE:&lt;#@;Y/^R^_CP=Z]E,B0F?$=:KG?*]D,R&lt;O=\6SH#H&lt;Z]CSQOM:,#ZBHS0,F?O:PMH.U$=Z&amp;*&gt;:=_B$V.[]::^9RZ6!&gt;QE3K.AK2I,27(%.W+:*!Z84J)':TVY$^C@6!*?P!&gt;;I!@"2:]/)3-'2X-N?W\D]_UL=Z+W#'!`%1PVUR3Q1!&lt;20U]7O9ZIO&gt;JXD8?Q/4^H&amp;LKPIR?\)Z"?\D=5P&gt;B::#J:/QV]MW"N8"L*`DW2\?#%TE1(H1`](*;A2VF&lt;^QL6C]D;WF@6RQ]Q9)2,-:I@:"S/`:@^C/&gt;+?23;PQ7DZ0@YT&amp;VPX(U4VZ7M!!!!!"!!!!&amp;Q!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!&amp;-1!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!")"5!A!!!!!!"!!A!-0````]!!1!!!!!""!!!!!=!#E!B"%FT4EE!!)!!]&gt;.L9VU!!!!$$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QZ%:8:J9W65?8"F,G.U&lt;!".1"9!"1N%;7&gt;J&gt;'&amp;M)%EP4Q^&amp;&lt;'6D&gt;(*P&lt;GFD)%RP971.4X.D?GFM&lt;'^T9W^Q:1.%45U'5G6M98FT!!!+2'6W;7.F6(FQ:1!!&amp;%!Q`````QJ7:7ZE&lt;X*/97VF!!!31$$`````#5VP:'6M4G&amp;N:1!51$$`````#U2F=W.S;8"U;7^O!":!-0````]-5W6S;7&amp;M4H6N9G6S!!!C1&amp;!!"A!!!!%!!A!$!!1!"1Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!1!'!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!$U6!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!9!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E6!)!!!!!!!1!&amp;!!=!!!%!!..NV"%!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'25!A!!!!!!"!!5!"Q!!!1!!UWX5%1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!&amp;!&amp;1#!!!!!!!%!#!!Q`````Q!"!!!!!!%E!!!!"Q!+1#%%38./31!!I!$RUWW[GQ!!!!--2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T$E2F&gt;GFD:62Z='5O9X2M!'V!&amp;A!(#U2J:WFU97QA33^0%E:V&lt;G.U;7^O)%&gt;F&lt;G6S982P=A^&amp;&lt;'6D&gt;(*P&lt;GFD)%RP971.4X.D?GFM&lt;'^T9W^Q:1.%45U-5'^X:8)A5X6Q='RZ"F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!71$$`````$&amp;.F=GFB&lt;%ZV&lt;7*F=A!!)E"1!!9!!!!"!!)!!Q!%!!5/2'6W;7.F,GRW9WRB=X-!!!%!"A!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'25!A!!!!!!"!!5!!Q!!!1!!!!!!%Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!%`&amp;1#!!!!!!!=!#E!B"%FT4EE!!+!!]&gt;.NOJM!!!!$$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QZ%:8:J9W65?8"F,G.U&lt;!"N1"9!"QN%;7&gt;J&gt;'&amp;M)%EP4R*'&gt;7ZD&gt;'FP&lt;C"(:7ZF=G&amp;U&lt;X)027RF9X2S&lt;WZJ9S"-&lt;W&amp;E$5^T9XJJ&lt;'RP=W.P='5$2%V.$&amp;"P&gt;W6S)&amp;.V=("M?1:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!&amp;E!Q`````QR4:8*J97R/&gt;7VC:8)!!#*!5!!'!!!!!1!#!!-!"!!&amp;$E2F&gt;GFD:3ZM&gt;G.M98.T!!!"!!9!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!#1!/!!!!"!!!!9I!!!!I!!!!!A!!"!!!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&lt;U!!!07?*S^5=NOUU!50?GY?@?6FP$KQ`W"AIL%CI7FBK))EE9N9I@%&gt;$SJ2JJY,-]Y*;TYDW\Y$H[D@!B]!&gt;SRL8&lt;"AI=%0N,)^]SV\THH!ND$*XS\HHW_!M#[!TF81B\IO6&lt;HKT?&amp;U.T;KHS^3/7"="KTK)^':[!OF/-[($Y[[2XHC8$+*/%,G=C-/Z/N0&gt;&gt;3O-QE3I3P$)^84KTYI,1W6JB5MM&amp;IV*W93ZG&amp;:XG;[E8^6'K_M%$\&gt;B96U8YQN/-BM"5^`EZ0_YV-9J/._9RO?S88'JF9[I+KODI$;57G5K])`:,LHMF-=4X/:_=S![2X`G8D6]YL.98LJ^%%&gt;&gt;4IES5Q"&amp;D?0&gt;+Z&gt;73"ZT:][&gt;NFEG:KTFX-H5S!:1,;C)(;6T3I9&amp;7**WCB&amp;48!JPI#T?AD-[?O*+3:9JUG-841R1L,YSH.WM?TPVN6CS3DC3)#CM)\7--[.EIQ^TYG22.SN)I?V0`;[W%6Z&gt;9@2,HED\I`;N\2/UICQ$&amp;J$^((89L8YZ\0N5+DQM`-\=X^!O6/;HBQ)_TX&gt;RR1\VP]O^TIVS"*42+ZC4NYC'XM6!*X[(W&lt;]NANR$.CAB]E_0U#!!!!!!!!C1!"!!)!!Q!'!!!!;!!0"!!!!!!0!.A!V1!!!(%!$Q1!!!!!$Q$9!.5!!!"[!!]%!!!!!!]!W!$6!!!!AQ!2B!#!!!!2!/]!Z!!!!)5!%I1!A!!!%1$P!/1!!!#(A!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!"-!%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!'KQ!!!2P!!!!)!!!'IQ!!!!!!!!!!!!!!#!!!!!U!!!%:!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B$1V.5!!!!!!!!!:R-38:J!!!!!!!!!&lt;"$4UZ1!!!!!!!!!=2544AQ!!!!!1!!!&gt;B%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!A!!!CBW:8*T!!!!"!!!!G241V.3!!!!!!!!!MB(1V"3!!!!!!!!!NR*1U^/!!!!!!!!!P"J9WQY!!!!!!!!!Q2$5%-S!!!!!!!!!RB-37:Q!!!!!!!!!SR'5%BC!!!!!!!!!U"'5&amp;.&amp;!!!!!!!!!V275%21!!!!!!!!!WB-37*E!!!!!!!!!XR#2%BC!!!!!!!!!Z"#2&amp;.&amp;!!!!!!!!![273624!!!!!!!!!\B%6%B1!!!!!!!!!]R.65F%!!!!!!!!!_")36.5!!!!!!!!!`271V21!!!!!!!!"!B'6%&amp;#!!!!!!!!""Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!O!!!!!!!!!!!`````Q!!!!!!!!$-!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!"8!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!,`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!!`````Q!!!!!!!!(U!!!!!!!!!!$`````!!!!!!!!!A1!!!!!!!!!!@````]!!!!!!!!$I!!!!!!!!!!#`````Q!!!!!!!!4=!!!!!!!!!!4`````!!!!!!!!"?!!!!!!!!!!"`````]!!!!!!!!&amp;^!!!!!!!!!!)`````Q!!!!!!!!9%!!!!!!!!!!H`````!!!!!!!!"BA!!!!!!!!!#P````]!!!!!!!!'+!!!!!!!!!!!`````Q!!!!!!!!9]!!!!!!!!!!$`````!!!!!!!!"F1!!!!!!!!!!0````]!!!!!!!!';!!!!!!!!!!!`````Q!!!!!!!!&lt;M!!!!!!!!!!$`````!!!!!!!!#P!!!!!!!!!!!0````]!!!!!!!!+_!!!!!!!!!!!`````Q!!!!!!!!NY!!!!!!!!!!$`````!!!!!!!!%D1!!!!!!!!!!0````]!!!!!!!!30!!!!!!!!!!!`````Q!!!!!!!"*%!!!!!!!!!!$`````!!!!!!!!%F1!!!!!!!!!!0````]!!!!!!!!3P!!!!!!!!!!!`````Q!!!!!!!",%!!!!!!!!!!$`````!!!!!!!!&amp;`Q!!!!!!!!!!0````]!!!!!!!!9"!!!!!!!!!!!`````Q!!!!!!!"A-!!!!!!!!!!$`````!!!!!!!!'$A!!!!!!!!!A0````]!!!!!!!!:`!!!!!!+2'6W;7.F,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!AR%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!+!!%!!!!!!!!"!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!1!!!!9!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!!Z!-0````]&amp;47^E:7Q!#E!B"%FT4EE!!&amp;9!]&gt;*,JL5!!!!#$E2F&gt;GFD:3ZM&gt;G.M98.T#E2F&gt;GFD:3ZD&gt;'Q!.%"1!!5!!!!"!!)!!Q!%(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!&amp;!!!!"@``````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!1!!!!A!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!!J!)12*=UZ*!!!?!$@`````!!E6!)!!!!!!!1!%!!!!!1!!!!!!!!!U1(!!&amp;12598.L!!!"!!5&amp;4EF%16%6!)!!!!!!!1!%!!!!!1!!!!!!!!B3:8.P&gt;8*D:1!!7!$RUEO^0!!!!!)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!W1&amp;!!"A!!!!%!!A!$!!1!"BZ$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"Q!!!!9!!!!!!!!!!1!!!!)!!!!$!!!!"0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!$!1!!!!A!#E!B"%FT4EE!!"Y!.`````]!#25!A!!!!!!"!!1!!!!"!!!!!!!!!$2!=!!6"&amp;2B=WM!!!%!!16/352"525!A!!!!!!"!!1!!!!"!!!!!!!!#&amp;*F=W^V=G.F!!!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!7!$RUEP"&lt;A!!!!)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!W1&amp;!!"A!!!!)!!Q!%!!5!"BZ$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"Q!!!!9!!!!%!!!!"1!!!!%!!!!#!!!!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"1!A!!!!!!!!!!!!!!!!!!"!!!!!!!%!1!!!!9!#E!B"%FT4EE!!$F!&amp;A!%#U2J:WFU97QA33^0$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A"7!0(34"&amp;@!!!!!AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QJ%:8:J9W5O9X2M!$2!5!!&amp;!!!!!1!#!!-!""Z$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"1!!!!5!!!!!!!!!!A!!!!-!!!!%!!!!"1!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!'!!J!)12*=UZ*!!!Z1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!6A$RUEQ28Q!!!!)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!U1&amp;!!"1!!!!%!!A!$!!1?1WRV=X2F=C"B&gt;8-A3WRB=X.F&lt;H"S;8:B&gt;'2B&gt;'6O!!!"!!5!!!!"`````A!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1!!!!!'!!J!)12*=UZ*!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!=!$RUWNC41!!!!--2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T$E2F&gt;GFD:62Z='5O9X2M!$V!&amp;A!%#U2J:WFU97QA33^0$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!'-!]&gt;.L9ME!!!!$$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QJ%:8:J9W5O9X2M!$2!5!!&amp;!!!!!1!#!!-!""Z$&lt;(6T&gt;'6S)'&amp;V=S",&lt;'&amp;T=W6O=(*J&gt;G&amp;U:'&amp;U:7Y!!!%!"1!!!!5!!!!!!!!!!A!!!!-!!!!%`````Q!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!!!!!1!!!!!!!A!!!!!'!!J!)12*=UZ*!!#!!0(4;W.&gt;!!!!!QR%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-/2'6W;7.F6(FQ:3ZD&gt;'Q!45!7!!5,2'FH;82B&lt;#"*,U]027RF9X2S&lt;WZJ9S"-&lt;W&amp;E$5^T9XJJ&lt;'RP=W.P='5$2%V."F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A"D!0(4;W.L!!!!!QR%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-+2'6W;7.F,G.U&lt;!!U1&amp;!!"1!!!!%!!A!$!!1?1WRV=X2F=C"B&gt;8-A3WRB=X.F&lt;H"S;8:B&gt;'2B&gt;'6O!!!"!!5!!!!&amp;!!!!!0````]!!!!"!!!!!A!!!!-!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!-!!!!!"Q!+1#%%38./31!!A!$RUWND81!!!!--2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T$E2F&gt;GFD:62Z='5O9X2M!%V!&amp;A!&amp;#U2J:WFU97QA33^0$U6M:7.U=G^O;7-A4'^B:!V0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!J%:8:J9W65?8"F!!!51$$`````#F:F&lt;G2P=EZB&lt;75!!"*!-0````]*47^E:7R/97VF!"2!-0````],2'6T9X*J=(2J&lt;WY!&amp;E!Q`````QR4:8*J97R/&gt;7VC:8)!!'5!]&gt;.MHY]!!!!$$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QJ%:8:J9W5O9X2M!$:!5!!'!!!!!1!#!!-!"!!&amp;(E.M&gt;8.U:8)A986T)%NM98.T:7ZQ=GFW982E982F&lt;A!!!1!'!!!!"A!!!!!!!!!"!!!!!A!!!!-!!!!%`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;!#!!!!!!!!!!!!!!!!!!!%!!!!!!!1!!!!!"Q!+1#%%38./31!!I!$RUWW[GQ!!!!--2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T$E2F&gt;GFD:62Z='5O9X2M!'V!&amp;A!(#U2J:WFU97QA33^0%E:V&lt;G.U;7^O)%&gt;F&lt;G6S982P=A^&amp;&lt;'6D&gt;(*P&lt;GFD)%RP971.4X.D?GFM&lt;'^T9W^Q:1.%45U-5'^X:8)A5X6Q='RZ"F*F&lt;'&amp;Z=Q!!#E2F&gt;GFD:62Z='5!!"2!-0````]+6G6O:'^S4G&amp;N:1!!%E!Q`````QF.&lt;W2F&lt;%ZB&lt;75!&amp;%!Q`````QN%:8.D=GFQ&gt;'FP&lt;A!71$$`````$&amp;.F=GFB&lt;%ZV&lt;7*F=A!!:1$RUWX5%1!!!!--2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T#E2F&gt;GFD:3ZD&gt;'Q!.E"1!!9!!!!"!!)!!Q!%!!5?1WRV=X2F=C"B&gt;8-A3WRB=X.F&lt;H"S;8:B&gt;'2B&gt;'6O!!!"!!9!!!!'!!!!!0````]!!!!#!!!!!Q!!!!1!!!!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!5!)!!!!!!!!!!!!!!!1!!!!Z%:8:J9W5O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Item Name="Device.ctl" Type="Class Private Data" URL="Device.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="DataOperation" Type="Folder">
		<Item Name="Description" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Description</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Description</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readDescription.vi" Type="VI" URL="../readDescription.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````],2'6T9X*J=(2J&lt;WY!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="writeDescription.vi" Type="VI" URL="../writeDescription.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#U2F=W.S;8"U;7^O!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="DeviceType" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">DeviceType</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">DeviceType</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readDeviceType.vi" Type="VI" URL="../readDeviceType.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!+!!]&gt;.NOJM!!!!$$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=QZ%:8:J9W65?8"F,G.U&lt;!"N1"9!"QN%;7&gt;J&gt;'&amp;M)%EP4R*'&gt;7ZD&gt;'FP&lt;C"(:7ZF=G&amp;U&lt;X)027RF9X2S&lt;WZJ9S"-&lt;W&amp;E$5^T9XJJ&lt;'RP=W.P='5$2%V.$&amp;"P&gt;W6S)&amp;.V=("M?1:3:7RB?8-!!!J%:8:J9W65?8"F!!!Y1(!!(A!!(1R%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-!%%2F&gt;GFD:3!I186T:W&amp;O:SE!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
			</Item>
			<Item Name="writeDeviceType.vi" Type="VI" URL="../writeDeviceType.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1#A!0(4&lt;&lt;K&lt;!!!!!QR%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-/2'6W;7.F6(FQ:3ZD&gt;'Q!&lt;5!7!!=,2'FH;82B&lt;#"*,U]32H6O9X2J&lt;WYA2W6O:8*B&gt;'^S$U6M:7.U=G^O;7-A4'^B:!V0=W.[;7RM&lt;X.D&lt;X"F!U2.41R1&lt;X&gt;F=C"4&gt;8"Q&lt;(E'5G6M98FT!!!+2'6W;7.F6(FQ:1!!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="IsNI" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">IsNI</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">IsNI</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readIsNI.vi" Type="VI" URL="../readIsNI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!)12*=UZ*!!!Y1(!!(A!!(1R%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-!%%2F&gt;GFD:3!I186T:W&amp;O:SE!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="writeIsNI.vi" Type="VI" URL="../writeIsNI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;%!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!+1#%%38./31!!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="Model" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Model</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Model</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readModelName.vi" Type="VI" URL="../readModelName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]&amp;47^E:7Q!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="writeModelName.vi" Type="VI" URL="../writeModelName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!/1$$`````"5VP:'6M!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="SerialNumber" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SerialNumber</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SerialNumber</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readSerialNumber.vi" Type="VI" URL="../readSerialNumber.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!-0````]-5W6S;7&amp;M4H6N9G6S!!!Y1(!!(A!!(1R%:8:J9W5O&lt;(:M;7)/2'6W;7.F,GRW9WRB=X-!%%2F&gt;GFD:3!I186T:W&amp;O:SE!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="writeSerialNumber.vi" Type="VI" URL="../writeSerialNumber.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71$$`````$&amp;.F=GFB&lt;%ZV&lt;7*F=A!!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%6J&lt;G&gt;B&lt;G=J!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="VendorName" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VendorName</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VendorName</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="readVendorName.vi" Type="VI" URL="../readVendorName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+6G6O:'^S4G&amp;N:1!!/%"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!""%:8:J9W5A+%&amp;V=W&gt;B&lt;G=J!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="writeVendorName.vi" Type="VI" URL="../writeVendorName.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B"&gt;8.H97ZH+1!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51$$`````#F:F&lt;G2P=EZB&lt;75!!$B!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!12'6W;7.F)#B&amp;;7ZH97ZH+1!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">32</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="DeviceType.ctl" Type="VI" URL="../DeviceType.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!\!!!!!1!T1"9!"!N%;7&gt;J&gt;'&amp;M)%EP4QV0=W.[;7RM&lt;X.D&lt;X"F!U2.41:3:7RB?8-!!!2&amp;&lt;H6N!!!"!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
	</Item>
	<Item Name="Init.vi" Type="VI" URL="../Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!&gt;$%2F&gt;GFD:3ZM&gt;GRJ9AZ%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!/2'6W;7.F)%&amp;V=W&gt;B&lt;G=!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"U-2'6W;7.F,GRW&lt;'FC$E2F&gt;GFD:3ZM&gt;G.M98.T!!Z%:8:J9W5A27FO:W&amp;O:Q!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
</LVClass>
