﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="Mein Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CodeSnippets" Type="Folder" URL="../CodeSnippets">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Obsolet" Type="Folder">
			<Item Name="Constructor_bak.vi" Type="VI" URL="../NiSwitch/Constructor_bak.vi"/>
			<Item Name="PXI1 Slot2.lvclass" Type="LVClass" URL="../PXI/PXI1 Slot2.lvclass"/>
		</Item>
		<Item Name="Sequence" Type="Folder" URL="../Sequence">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="NiSwitch.lvclass" Type="LVClass" URL="../NiSwitch/NiSwitch.lvclass"/>
		<Item Name="Relay.lvclass" Type="LVClass" URL="../Relay/Relay.lvclass"/>
		<Item Name="Relays.lvclass" Type="LVClass" URL="../Relays/Relays.lvclass"/>
		<Item Name="VB_DMM.lvclass" Type="LVClass" URL="../NiVirtuelBench/VB_DMM/VB_DMM.lvclass"/>
		<Item Name="VB_Oszilloskop.lvclass" Type="LVClass" URL="../NiVirtuelBench/VB_Oszilloskop/VB_Oszilloskop.lvclass"/>
		<Item Name="VirtuelBench.lvclass" Type="LVClass" URL="../NiVirtuelBench/VirtuelBench.lvclass"/>
		<Item Name="Abhängigkeiten" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niSwitch Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Close.vi"/>
				<Item Name="niSwitch Get Relay Count.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Get Relay Count.vi"/>
				<Item Name="niSwitch Get Relay Name.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Get Relay Name.vi"/>
				<Item Name="niSwitch Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Initialize.vi"/>
				<Item Name="niSwitch Relay Action.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Relay Action.ctl"/>
				<Item Name="niSwitch Relay Control.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Relay Control.vi"/>
				<Item Name="niSwitch Switch Is Debounced.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Switch Is Debounced.vi"/>
				<Item Name="niSwitch Topologies.ctl" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.LLB/niSwitch Topologies.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="_DMM Configure Measurement0.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/_DMM Configure Measurement0.vi"/>
				<Item Name="_MSO Configure Analog Channel0.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/_MSO Configure Analog Channel0.vi"/>
				<Item Name="_MSO Configure Timing0.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/_MSO Configure Timing0.vi"/>
				<Item Name="DMM Close.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Close.vi"/>
				<Item Name="DMM Configure AC Current.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Configure AC Current.vi"/>
				<Item Name="DMM Configure DC Current.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Configure DC Current.vi"/>
				<Item Name="DMM Configure DC Voltage.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Configure DC Voltage.vi"/>
				<Item Name="DMM Configure Measurement.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Configure Measurement.vi"/>
				<Item Name="DMM Export Configuration.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Export Configuration.vi"/>
				<Item Name="DMM Import Configuration.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Import Configuration.vi"/>
				<Item Name="DMM Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Initialize.vi"/>
				<Item Name="DMM Read.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Read.vi"/>
				<Item Name="DMM Reset Instrument.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/DMM Reset Instrument.vi"/>
				<Item Name="LVComboBoxStrsAndValuesArrayTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVComboBoxStrsAndValuesArrayTypeDef.ctl"/>
				<Item Name="MSO Autosetup.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Autosetup.vi"/>
				<Item Name="MSO Close.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Close.vi"/>
				<Item Name="MSO Configure Analog Channel.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Configure Analog Channel.vi"/>
				<Item Name="MSO Configure Timing.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Configure Timing.vi"/>
				<Item Name="MSO Enable Digital Channels.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Enable Digital Channels.vi"/>
				<Item Name="MSO Initialize.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Initialize.vi"/>
				<Item Name="MSO Read.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Read.vi"/>
				<Item Name="MSO Run.vi" Type="VI" URL="/&lt;vilib&gt;/VirtualBench/MSO Run.vi"/>
			</Item>
			<Item Name="Device.lvclass" Type="LVClass" URL="../Device/Device.lvclass"/>
		</Item>
		<Item Name="Build-Spezifikationen" Type="Build"/>
	</Item>
</Project>
