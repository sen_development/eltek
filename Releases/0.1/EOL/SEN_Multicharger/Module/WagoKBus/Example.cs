﻿using System;
using System.Diagnostics;

namespace WAGO.PMB.Feldbus
{
  class Program
  {
    static void Main( string[ ] args )
    {
      EthernetIO_NET dev = null;

      EthernetIO.IOInfo info;

      var regNo   = new byte[ ] { 37, 38 };

      var regData = new ushort[ ] { 0x0000, 0x0000 };
      var cmpData = new ushort[ ] { 0x0000, 0x0000 };

      ushort[ ] ident;

      int numOfIOs;



      // ---------------------------------------------------------------------- //
      try
      { // Objekt instantiieren und Verbindung aufbauen
        dev    = new EthernetIO_NET( "192.168.10.101" );
        // Aktuelle Koppplerdaten ermitteln
        info = dev.GetIOInfo( 0 );
        // Anzahl Baugruppen ermitteln
        numOfIOs = dev.GetCouplerConfig( out ident );

        // Alle Baugruppen durchlaufen
        for( byte i = 1; i <= numOfIOs; i++ )
        { // Aktuelle Baugruppendaten ermitteln
          info = dev.GetIOInfo( i );
          
          // Alle Kanäle durchlaufen
          for( byte j = 0; j < info.channels; j++ )
          { // Berechne Registerwerte
            regData[ 0 ] = ( ushort )( j * 100 );
            regData[ 1 ] = ( ushort )( j * 200 );
            // Schreibe Registerwert
            dev.WriteRegister( i, ( ENTable )j, regNo, regData );
          }
        }

        // Reset ausführen
        dev.Reset( ENReset.SW );

        // Alle Baugruppen durchlaufen
        for( byte i = 1; i <= numOfIOs; i++ )
        { // Aktuelle Baugruppendaten ermitteln
          info = dev.GetIOInfo( i );
          
          // Alle Kanäle durchlaufen
          for( byte j = 0; j < info.channels; j++ )
          { // Berechne geschriebene Registerwerte
            regData[ 0 ] = ( ushort )( j * 100 );
            regData[ 1 ] = ( ushort )( j * 200 );               
            // Lese Registerwert
            cmpData = dev.ReadRegister( i, ( ENTable )j, regNo );

            // Vergleiche geschriebene / gelesene Wert 
            if      ( regData[ 0 ] != cmpData[ 0 ] )
            {
              Debug.WriteLine( "-----------------------------------" );
              Debug.WriteLine( "Baugruppe : " + i.ToString( ) + ", Kanal : " + j.ToString( ) + ", Register : 37" );
              Debug.WriteLine( "Sollwert  : " + regData[ 0 ].ToString( ) + ", Istwert : " + cmpData[ 0 ].ToString( ));
              Debug.WriteLine( "-----------------------------------" );
            }
            // Vergleiche geschriebene / gelesene Wert 
            else if ( regData[ 1 ] != cmpData[ 1 ] )
            {
              Debug.WriteLine( "-----------------------------------" );
              Debug.WriteLine( "Baugruppe : " + i.ToString( ) + ", Kanal : " + j.ToString( ) + ", Register : 38" );
              Debug.WriteLine( "Sollwert  : " + regData[ 1 ].ToString( ) + ", Istwert : " + cmpData[ 1 ].ToString( ));
              Debug.WriteLine( "-----------------------------------" );
            }

          }
        }
      }
      // ---------------------------------------------------------------------- //
      catch( Exception ex )
      {
        // Fehler ausgeben
        Debug.WriteLine( ex.Message );
      }
      // ---------------------------------------------------------------------- //
      finally
      { // Verbindung trennen
        if( dev != null )
          dev.Disconnect( );
      }
      // ---------------------------------------------------------------------- //
    }
  }
}
